var debugging = true;

var engine = (function () {
	return {
		tileWidth: 32,
		tileHeight: 32,
		boards: [],
		debug: function (txt) {
			if (debugging) {
				console.log("- " + txt);
			};
		},
		tile: function (img) {
			//
			var img = img || "img/dev.png"
			return {
				image: img
			};
		},
		board: function () {
			//
			var tmp = [];
			var tmp1 = [];
			var tmp2 = [];
			var tmp3 = engine.tile();
			for (b=0;b<=63;b++)
			{
				tmp2.push(tmp3);
			};
			for (a=0;a<=47;a++)
			{
				tmp1.push(tmp2);
			};
			tmp.push(tmp1);
			tmp.push(tmp1);
			return tmp;
		},
		sys: (function () {
			return {
				getMaxResolution: function () {
					engine.debug("Entering: engine.sys.getMaxResolution");
					//
					var w = [];
					w[0] = screen.availWidth || 999999999;
					w[1] = window.innerWidth || 999999999;
					w[2] = document.body.clientWidth || 999999999;
					w.sort(function(a, b) {
				    	return a - b;
					});

					var h = [];
					h[0] = screen.availHeight || 999999999;
					h[1] = window.innerHeight || 999999999;
					h[2] = document.body.clientHeight || 999999999;
					h.sort(function(a, b) {
				    	return a - b;
					});
					//
						engine.debug("--- Resolution Width: " + w);
						engine.debug("--- Resolution Height: " + h);
						engine.debug("Exiting: engine.sys.getMaxResolution");
					//
					return [w[0],h[0]];
				},
				createBoard: function (zPos) {
					engine.debug("Entering: engine.sys.createBoard");
					//
					var zPos = zPos || 0;
					engine.boards.push(zPos);
					//
					var bTable = document.createElement("table");
					for (a=0;a<=47;a++)
					{
						var bTR = document.createElement("tr");
							bTR.id = "Z" + zPos + "R" + a;
						for (b=0;b<=63;b++)
						{
							var bTD = document.createElement("td");
								bTD.id = "Z" + zPos + "R" + a + "C" + b;
								bTD.setAttribute("align","left");
								bTD.setAttribute("valign","top");
								//bTD.setAttribute("style","width:64px; height:64px;");
							var bIMG = document.createElement("img");
								bIMG.id = "Z" + zPos + "X" + a + "Y" + b;
								bIMG.src = "img/blank.png";
								bIMG.width = engine.tileWidth;
								bIMG.height = engine.tileHeight;
							bTD.appendChild(bIMG);
							bTR.appendChild(bTD);
						};
						bTable.appendChild(bTR);
					};
					bTable.id = "Z" + zPos;
					bTable.setAttribute("style", "width:100%; height:100%; border:0px");
					bTable.setAttribute("cellpadding", "0px");
					bTable.setAttribute("cellspacing", "0px");
					document.body.appendChild(bTable);
					//
					engine.debug("Exiting: engine.sys.createBoard");
				},
				adjustToResChange: function (e) {
					engine.debug("Entering: engine.sys.adjustToResChange");
					//
					engine.sys.determineTileSize();
					//
					for (z=0;z<=engine.boards.length - 1;z++)
					{
						for (a=0;a<=47;a++)
						{
							//
							for (b=0;b<=63;b++)
							{
								//
								var bIMG = document.getElementById("Z" + z + "X" + a + "Y" + b);
								bIMG.width = engine.tileWidth;
								bIMG.height = engine.tileHeight;
							};
						};
					};
					//				
						engine.debug("New Tile Dimensions: " + engine.tileWidth + "x" + engine.tileHeight);
					//
					//
					engine.debug("Exiting: engine.sys.adjustToResChange");
				},
				determineTileSize: function () {
					engine.debug("Entering: engine.sys.determineTileSize");
					//
					engine.resolution = engine.sys.getMaxResolution();
					//
					var w = [];
					w[0] = Math.ceil(engine.resolution[0] / 32);
					w[1] = Math.ceil(engine.resolution[1] / 24);
					w.sort(function(a, b) {
				    	return a - b;
					});
					//
					engine.tileWidth = w[1];
					engine.tileHeight = w[1];
					//
					engine.debug("Exiting: engine.sys.determineTileSize");
				},
				fillBoard: function (data) {
					engine.debug("Entering: engine.sys.fillBoard");
					//
					for (z=0;z<=data.length - 1;z++)
					{
						for (a=0;a<=47;a++)
						{
							for (b=0;b<=63;b++)
							{
								//
							}
						}
					}
					//
					engine.debug("Exiting: engine.sys.fillBoard");
				},
				input: (function () {
					return {
						mouse: function (e) {
							//
							//engine.debug("Mouse Event");
							e.preventDefault();
						},
						keyboard: function (e) {
							//
							// engine.debug("Keyboard Event");
							e.preventDefault();
						},
						touch: function (e) {
							//
							// engine.debug("Touch Event");
							e.preventDefault();
						},
						gamepad: (function () {
							return {
								connect: function (e) {
									//
								},
								disconnect: function (e) {
									//
								},
								poll: function () {
									//
									for (a=0;a<=navigator.getGamepads()[0].buttons.length;a++)
									{
										if (typeof(navigator.getGamepads()[0].buttons[a]) == "object")
										{
											if (navigator.getGamepads()[0].buttons[a].pressed)
											{
												engine.debug("Gamepad Button: " + a);	
											};
										};
									};
									for (a=0;a<=navigator.getGamepads()[0].axes.length;a++)
									{
										if (navigator.getGamepads()[0].axes[a] != engine.sys.input.gamepad.zeroedAxes[a])
										{
											engine.debug("Axis " + a + ": " + navigator.getGamepads()[0].axes[a].toFixed());
										};
									};
								},
								zeroAxes: function () {
									//
									engine.sys.input.gamepad.zeroedAxes = navigator.getGamepads()[0].axes;
								}
							};
						}())
					}
				}())
			}
		}()),
		init: function () {
			engine.debug("Entering: engine.init");
			//
			// engine.sys.createBoard(0);
			// engine.sys.createBoard(1);
			engine.sys.adjustToResChange();
			//				
				engine.debug("Tile Dimensions: " + engine.tileWidth + "x" + engine.tileHeight);
			//
			engine.level1 = engine.board();
			engine.sys.fillBoard(engine.level1);
			var tmp = document.getElementById("loading");
			tmp.setAttribute("style","display:none;");
			//
			if (navigator.getGamepads()[0])
			{
				engine.debug("Gamepad probably present: " + navigator.getGamepads()[0].id);
				engine.sys.input.gamepad.zeroAxes();
				engine.gamepadPollInterval = setInterval(engine.sys.input.gamepad.poll, 50);
			}
			//
			// Register Events
			// document.body.addEventListener("keydown", engine.sys.input.keyboard, true);
			// document.body.addEventListener("keyup", engine.sys.input.keyboard, true);
			// document.body.addEventListener("keypress", engine.sys.input.keyboard, true);
			// //
			document.body.addEventListener("mousedown", engine.sys.input.mouse, true);
			document.body.addEventListener("mouseup", engine.sys.input.mouse, true);
			document.body.addEventListener("mousemove", engine.sys.input.mouse, true);
			// //
			document.body.addEventListener("touchstart", engine.sys.input.touch, true);
			document.body.addEventListener("touchend", engine.sys.input.touch, true);
			document.body.addEventListener("touchcancel", engine.sys.input.touch, true);
			document.body.addEventListener("touchleave", engine.sys.input.touch, true);
			document.body.addEventListener("touchmove", engine.sys.input.touch, true);
			//
			window.addEventListener("gamepadconnected", engine.sys.input.gamepad.connect);
			window.addEventListener("gamepaddisconnected", engine.sys.input.gamepad.disconnect);
			//
			window.addEventListener("resize", engine.sys.adjustToResChange, true);
			//
			engine.debug("Exiting: engine.init");
		}
	};
}());